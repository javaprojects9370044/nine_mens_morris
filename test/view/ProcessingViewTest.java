import static org.junit.Assert.*;

import java.util.ArrayList;


import org.junit.Test;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import controller.IController;
import model.SimpleResult;
import controller.IController;

public class ProcessingViewTest {

  private ProcessingView view;

  @Before
  public void setUp() {
    view = new ProcessingView(800, 600, 50); 
  }

  @Test
  public void testFindTokenPosition() {
    int index = view.findTokenPosition(100, 100);
    assertEquals(0, index);

    index = view.findTokenPosition(500, 500); 
    assertEquals(-1, index);
  }

  @Test 
  public void testUpdateTokens() {
    ArrayList<SimpleResult<Integer, Integer>> pieces = new ArrayList<>();
    pieces.add(new SimpleResult<>(0, 1));
    pieces.add(new SimpleResult<>(5, 2));

    view.updateTokens(pieces);

    assertEquals(2, view.playerTokens.size());
    assertEquals(1, view.playerTokens.get(0).y());
    assertEquals(2, view.playerTokens.get(1).y());
  }
  @Mock
  private IController controller;
  
  private ProcessingView view;

  @Before
  public void setUp() {
    view = new ProcessingView(800, 600, 50);
    view.setController(controller);
  }

  @Test
  public void testSetController() {
    verify(view, times(1)).setController(controller);
  }

  @Test
  public void testFindTokenPositionOutOfBounds() {
    int index = view.findTokenPosition(-100, -100);
    assertEquals(-1, index);
    
    index = view.findTokenPosition(1000, 1000);
    assertEquals(-1, index);
  }

  @Test
  public void testUpdateTokensConcurrency() throws InterruptedException {
    ArrayList<SimpleResult<Integer, Integer>> pieces = new ArrayList<>();
    pieces.add(new SimpleResult<>(0, 1));

    view.updateTokens(pieces);

    // Give time for thread to finish
    Thread.sleep(100);

    assertEquals(1, view.playerTokens.size());
  }
  
  @Test
  public void testDrawText() {
    view.drawText("Test", 0.1, 0.5, 0.5);
    
    // Verify drawText called with expected params
    verify(view, times(1)).drawText("Test", 0.1, 0.5, 0.5); 
  }
}
