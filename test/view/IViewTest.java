import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import view.IView;

class IViewTest {

  @Test
  void testDrawJumpingOrMoving_validPlayer() {
    IView view = new IView() {
      @Override
      public void draw_jumping_or_moving(String playerName) {
        // View drawing logic
      }
    };

    assertDoesNotThrow(() -> {
      view.draw_jumping_or_moving("Alice");
    });
  }

  @Test
  void testDrawJumpingOrMoving_emptyPlayer() {
    IView view = new IView() {
      @Override
      public void draw_jumping_or_moving(String playerName) {
        // View drawing logic  
      }
    };

    assertThrows(IllegalArgumentException.class, () -> {
      view.draw_jumping_or_moving("");
    });
  }

  @Test
  void testDrawJumpingOrMoving_nullPlayer() {
    IView view = new IView() {
      @Override
      public void draw_jumping_or_moving(String playerName) {
        // View drawing logic
      }
    };

    assertThrows(NullPointerException.class, () -> {
      view.draw_jumping_or_moving(null);
    });
  }
}
