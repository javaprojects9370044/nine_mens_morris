import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.jupiter.api.Test;
import view.Blackscreen;

public class BlackscreenTest {

  @Test
  public void testConstructor() {
    Blackscreen blackscreen = new Blackscreen(640, 480, 10);
    
    assertEquals(0, blackscreen.backgroundColor);
    assertEquals(255, blackscreen.foregroundColor); 
  }

  @Test
  public void testWidthHeight() {
    Blackscreen blackscreen = new Blackscreen(1024, 768, 5);
    
    assertEquals(1024, blackscreen.width);
    assertEquals(768, blackscreen.height);
  }

  @Test 
  public void testTokenSize() {
    Blackscreen blackscreen = new Blackscreen(800, 600, 20);
    
    assertEquals(20, blackscreen.tokenSize, 0);
  }
}
