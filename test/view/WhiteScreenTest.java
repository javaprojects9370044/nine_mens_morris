import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.jupiter.api.Test;
import view.WhiteScreen;

public class WhiteScreenTest {

  @Test
  public void testConstructor() {
    WhiteScreen whiteScreen = new WhiteScreen(500, 500, 10f);
    
    assertEquals(255, whiteScreen.backgroundColor);
    assertEquals(0, whiteScreen.frontgroundColor); 
  }

  @Test 
  public void testDifferentSizes() {
    WhiteScreen whiteScreen1 = new WhiteScreen(100, 200, 5f);
    WhiteScreen whiteScreen2 = new WhiteScreen(300, 400, 15f);
    
    assertNotEquals(whiteScreen1.width, whiteScreen2.width);
    assertNotEquals(whiteScreen1.height, whiteScreen2.height);
    assertNotEquals(whiteScreen1.tokenSize, whiteScreen2.tokenSize);
  }

}
