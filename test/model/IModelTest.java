import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;
import org.junit.Test;

import model.Player;
import model.IModel;

public class IModelTest {

  @Test
  public void testOpponentPlayer_default() {
    IModel model = new ModelImpl(); // implementation with default players
    Player opponent = model.opponent_player();

    assertNotNull(opponent);
    assertEquals("Player 2", opponent.getName()); 
  }

  @Test 
  public void testOpponentPlayer_custom() {
    Player player1 = new Player("Alice");
    Player player2 = new Player("Bob");
    IModel model = new ModelImpl(player1, player2);

    Player opponent = model.opponent_player();

    assertNotNull(opponent);
    assertEquals("Bob", opponent.getName());
  }

}

import model.Player;
import model.IModel;

public class IModelTest {

  @Test
  public void testOpponentPlayer_default() {
    IModel model = new ModelImpl(); // implementation with default players
    Player opponent = model.opponent_player();

    assertNotNull(opponent);
    assertEquals("Player 2", opponent.getName()); 
  }

  @Test 
  public void testOpponentPlayer_custom() {
    Player player1 = new Player("Alice");
    Player player2 = new Player("Bob");
    IModel model = new ModelImpl(player1, player2);

    Player opponent = model.opponent_player();

    assertNotNull(opponent);
    assertEquals("Bob", opponent.getName());
  }

}
