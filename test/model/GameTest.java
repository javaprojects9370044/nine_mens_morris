package model;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Game;
import model.GameStatus;
import model.Move;
import model.SimpleResult;

class GameTest {

  @Test
  void testCheckWinCondition_blackWon() {
    // Arrange
    Game game = new Game(GameStatus.InProgress, /* currentPlayer */, /* opponentPlayer */);
    
    // Act
    Game result = game.checkWinCondition();
    
    // Assert
    assertEquals(GameStatus.BlackWon, result.status()); 
  }

  @Test
  void testCheckWinCondition_whiteWon() {
    // Arrange
    Game game = new Game(GameStatus.InProgress, /* currentPlayer */, /* opponentPlayer */);
    
    // Act
    Game result = game.checkWinCondition();
    
    // Assert
    assertEquals(GameStatus.WhiteWon, result.status());
  }

  @Test
  void testIsGameOver_trueForBlackWon() {
    // Arrange
    Game game = new Game(GameStatus.BlackWon, /* currentPlayer */, /* opponentPlayer */);
    
    // Act
    boolean result = game.isGameOver();
    
    // Assert
    assertTrue(result);
  }

  @Test
  void testIsGameOver_trueForWhiteWon() {
    // Arrange
    Game game = new Game(GameStatus.WhiteWon, /* currentPlayer */, /* opponentPlayer */);
    
    // Act
    boolean result = game.isGameOver();
    
    // Assert 
    assertTrue(result);
  }

  @Test
  void testIsGameOver_falseForInProgress() {
    // Arrange
    Game game = new Game(GameStatus.InProgress, /* currentPlayer */, /* opponentPlayer */);
    
    // Act
    boolean result = game.isGameOver();
    
    // Assert
    assertFalse(result);
  }
  
  @Test
  void testMakeAIMove() {
    // Arrange
    Game game = Game.of();
    
    // Act
    SimpleResult<Game, Move> result = game.makeAIMove();
    
    // Assert
    assertNotNull(result);
    assertNotNull(result.value());
  }

  // Additional tests for move_or_jump

@Test
void testMoveOrJump_validMove() {
  // Arrange
  Game game = Game.of();
  int newPosition = 1;
  int oldPosition = 2; 
  
  // Act
  SimpleResult<Game, Move> result = game.move_or_jump(newPosition, oldPosition);

  // Assert
  assertNotNull(result);
  assertNotNull(result.value());
  assertEquals(newPosition, result.value().currentPlayer().piecePositions().get(0));
}

@Test 
void testMoveOrJump_invalidMove() {
  // Arrange
  Game game = Game.of();
  int newPosition = 1; 
  int oldPosition = 1;

  // Act
  SimpleResult<Game, Move> result = game.move_or_jump(newPosition, oldPosition);

  // Assert
  assertNull(result.value());
}


// Additional tests for remove_opponent_piece

@Test
void testRemoveOpponentPiece() {
  // Arrange
  Game game = Game.of();
  int position = 1;

  // Act
  SimpleResult<Game, Move> result = game.remove_opponent_piece(position);

  // Assert
  assertNotNull(result);
  assertFalse(result.value().opponentPlayer().piecePositions().contains(position));
}

// Additional tests for place_piece

@Test
void testPlacePiece() {
  // Arrange 
  Game game = Game.of();
  int position = 1;

  // Act
  SimpleResult<Game, Move> result = game.place_piece(position);

  // Assert
  assertNotNull(result);
  assertTrue(result.value().currentPlayer().piecePositions().contains(position));
}


}
