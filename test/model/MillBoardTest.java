package model;

import model.MillBoard;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MillBoardTest {

  @Test
  void testOf() {
    MillBoard board = MillBoard.of();
    assertNotNull(board);
    assertEquals(24, board.board().length());
  }

  @Test
  void testSetBoard() {
    MillBoard board = MillBoard.of();
    board = board.setBoard(5, true);
    assertTrue(board.is_used(5));
  }

  @Test
  void testCheckPossibleMills() {
    MillBoard board = MillBoard.of();
    board = board.setBoard(1, true);
    board = board.setBoard(2, true);
    board = board.setBoard(3, true);
    assertTrue(board.checkPossibleMills());
  }

  @Test
  void testIsUsed() {
    MillBoard board = MillBoard.of();
    board = board.setBoard(5, true);
    assertTrue(board.is_used(5));
    assertFalse(board.is_used(10));
  }

  @Test
  void testDangerZone() {
    MillBoard board = MillBoard.of();
    board = board.setBoard(0, true);
    board = board.setBoard(1, true);
    board = board.setBoard(2, true);
    assertFalse(board.dangerZone());
    board = board.setBoard(3, true);
    assertTrue(board.dangerZone());
  }

  @Test
  void testDeadZone() {
    MillBoard board = MillBoard.of();
    board = board.setBoard(0, true);
    board = board.setBoard(1, true);
    assertFalse(board.deadZone());
    board = board.setBoard(2, true);
    assertTrue(board.deadZone());
  }

  @Test
  void testIsInMill() {
    MillBoard board = MillBoard.of();
    board = board.setBoard(1, true);
    board = board.setBoard(2, true);
    board = board.setBoard(3, true);
    assertTrue(board.isInMill(2));
    assertFalse(board.isInMill(5));
  }

  @Test
  void testIsInCorner() {
    assertTrue(MillBoard.of().is_in_corner(0));
    assertFalse(MillBoard.of().is_in_corner(5));
  }

}
