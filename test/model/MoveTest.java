import static org.junit.Assert.*;

import java.util.stream.IntStream;

import org.junit.Test;
import org.junit.jupiter.api.Test;

import model.Game;
import model.GamePhase;
import model.GameStatus;
import model.Move;

public class MoveTest {

  @Test
  public void testIsValidPlacing() {
    Game gameOver = new Game(GameStatus.BlackWon); 
    Game validPos = new Game(); // position open

    assertEquals(Move.IMPOSSIBLE_MOVE, Move.is_valid_placing(gameOver, 5));
    assertEquals(Move.POSSIBLE_MOVE, Move.is_valid_placing(validPos, 5));
  }

  @Test 
  public void testIsValidJump() {
    Game gameOver = new Game(GameStatus.WhiteWon);
    Game validJump = new Game(); // open position

    assertEquals(Move.IMPOSSIBLE_MOVE, Move.is_valid_jump(gameOver, 5, 3)); 
    assertEquals(Move.POSSIBLE_MOVE, Move.is_valid_jump(validJump, 5, 3));
  }

  @Test
  public void testIsValidMove() {
    Game gameOver = new Game(GameStatus.BlackWon);
    Game validMove = new Game(); // open position

    assertEquals(Move.IMPOSSIBLE_MOVE, Move.is_valid_move(gameOver, 5, 3));
    assertEquals(Move.POSSIBLE_MOVE, Move.is_valid_move(validMove, 5, 3)); 
  }

  @Test
  public void testPiecePlacementExecution() {
    Game impossiblePlace = new Game(); // position filled
    Game possiblePlace = new Game(); // open position

    assertEquals(Move.IMPOSSIBLE_MOVE, Move.piece_place_execution(impossiblePlace, 5).move());
    assertEquals(Move.POSSIBLE_MOVE, Move.piece_place_execution(possiblePlace, 5).move());
  }

  @Test
  public void testMoveExecution() {
    Game impossibleMove = new Game(); // invalid move
    Game possibleMove = new Game(); // valid move

    assertEquals(Move.IMPOSSIBLE_MOVE, Move.move_ore_jump_execution(impossibleMove, 5, 3).move());
    assertEquals(Move.POSSIBLE_MOVE, Move.move_ore_jump_execution(possibleMove, 5, 3).move());
  }

  @Test
public void testBoardWrapper() {
  assertEquals(0, Move.board_wrapper(24));
  assertEquals(5, Move.board_wrapper(29));
}

@Test
public void testIsInCorner() {
  assertTrue(Move.is_in_corner(0));
  assertTrue(Move.is_in_corner(2));
  assertFalse(Move.is_in_corner(3));
} 

@Test
public void testCheckGameStatus() {
  Game inProgress = new Game(GameStatus.InProgress);
  Game blackWon = new Game(GameStatus.BlackWon);

  assertEquals(Move.POSSIBLE_MOVE, Move.check_game_status(inProgress));
  assertEquals(Move.IMPOSSIBLE_MOVE, Move.check_game_status(blackWon)); 
}

@Test
public void testGeneratePossibleMoves() {
  Game game = new Game();
  
  // Validate moves for different phases
  assertEquals(24, Move.generatePossibleMoves(game).size()); // Placing phase
  game.setCurrentPhase(GamePhase.Moving);
  assertEquals(8, Move.generatePossibleMoves(game).size()); // Moving phase
}

@Test 
public void testRemovePossiblePieces() {
  Game game = new Game();

  // Validate removal expansion
  assertEquals(3, Move.removePossiblePieces(game, 5).size()); 
}
// More parameter value edge cases

@Test 
public void testBoardWrapEdgeCases() {
  assertEquals(0, Move.board_wrapper(Integer.MIN_VALUE));
  assertEquals(0, Move.board_wrapper(Integer.MAX_VALUE));
}

@Test
public void testIsInCornerEdgeCases() {
  assertFalse(Move.isInCorner(-1));
  assertFalse(Move.isInCorner(24));
}

// Mocking Game class

Game mockGame = Mockito.mock(Game.class);
Mockito.when(mockGame.getStatus()).thenReturn(GameStatus.InProgress);

@Test
public void testCheckGameStatusWithMock() {
  assertEquals(Move.POSSIBLE_MOVE, Move.checkGameStatus(mockGame)); 
}

// More assertions

@Test
public void testMoveExecutionSideEffects() {
  Game game = new Game();
  int startPieces = game.getPieces().size();

  SimpleResult result = Move.moveExecution(game, 5, 3);

  assertEquals(Move.POSSIBLE_MOVE, result.getMove());
  assertEquals(startPieces, game.getPieces().size()); // Piece count unchanged
}

// Test class interactions

@Test
public void testGameAndMoveResult() {
  Game game = new Game();
  GameAndMove result = Move.moveExecution(game, 5, 3);

  assertEquals(game, result.getGame());
  assertEquals(Move.POSSIBLE_MOVE, result.getMove());
}

// Concurrency testing

@Test
public void testThreadSafety() {
  Game game = new Game();

  IntStream.range(0, 100).parallel()
    .forEach(i -> Move.isValidMove(game, i, i+1)); 

  assertFalse(game.hasErrors());
}


}
