import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Game;
import model.Game_Artificial_Intelligence;
import model.Move;
import model.Simple_Result;

class GameAITest {

  @Test
  void testMakeBestMove() {
    // Arrange
    Game game = new Game();
    Game_Artificial_Intelligence ai = new Game_Artificial_Intelligence(game);
    
    // Act
    Simple_Result<Game, Move> result = ai.make_best_move();
    
    // Assert
    assertNotNull(result);
    assertNotEquals(Move.IMPOSSIBLE_MOVE, result.move());
  }

  @Test
  void testRateMove() {
    // Arrange
    Game game = new Game();
    Game_Artificial_Intelligence ai = new Game_Artificial_Intelligence(game);
    Simple_Result<Game, Move> move = new Simple_Result<>(game, Move.POSSIBLE_MOVE);
    
    // Act
    int rating = ai.rate_move(move);
    
    // Assert
    assertEquals(1, rating);
  }

}
