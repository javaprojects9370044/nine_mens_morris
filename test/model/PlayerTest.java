import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import model.Player;
import model.GamePhase;
import model.Color;

public class PlayerTest {

  @Test
  public void testSetPiece() {
    Player player = new Player(Color.WHITE, GamePhase.Placing, new MillBoard(), 0); 
    Player updatedPlayer = player.setPiece(1);
    
    assertEquals(GamePhase.Placing, updatedPlayer.state());
    assertEquals(1, updatedPlayer.counter());
  }

  @Test 
  public void testSetPieceInvalidPhase() {
    Player player = new Player(Color.WHITE, GamePhase.Moving, new MillBoard(), 9);
    Player updatedPlayer = player.setPiece(1);
    
    assertEquals(GamePhase.Moving, updatedPlayer.state());
    assertEquals(9, updatedPlayer.counter());
  }

  @Test
  public void testRemovePiece() {
    MillBoard board = new MillBoard();
    board.setBoard(1, true);
    Player player = new Player(Color.BLACK, GamePhase.Moving, board, 9);
    
    Player updated = player.removePiece(1);
    
    assertFalse(updated.board().getBoard().get(1));
    assertEquals(GamePhase.Moving, updated.state());
  }

  @Test
  public void testRemovePieceEndGame() {
    MillBoard board = new MillBoard();
    board.setBoard(1, true);
    Player player = new Player(Color.BLACK, GamePhase.Moving, board, 2);
    
    Player updated = player.removePiece(1);
    
    assertFalse(updated.board().getBoard().get(1));
    assertEquals(GamePhase.EndGame, updated.state()); 
  }

  @Test
  public void testGetPieceAmount() {
    MillBoard board = new MillBoard();
    board.setBoard(1, true);
    board.setBoard(2, true);
    
    Player player = new Player(Color.WHITE, GamePhase.Placing, board, 0);
    
    assertEquals(2, player.getPieceAmount());
  }
}
