package model;

import org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
import org.junit.jupiter.api.Test;
import model.Color;

public class ColorTest {

  @Test
  public void testColorValues() {
    assertEquals(Color.BLACK, Color.BLACK);
    assertEquals(Color.WHITE, Color.WHITE);
  }
}
