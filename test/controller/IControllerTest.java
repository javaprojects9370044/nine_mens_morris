package controller;

import static org.junit.Assert.*;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import controller.IController;

public class IControllerTest {

  @Test
  public void testNextFrame() {
    IController controller = new MockController(); 
    controller.nextFrame();
    
    // Assert no exceptions
  }

  @Test 
  public void testUserInputValid() {
    IController controller = new MockController();
    controller.userInput(10, 20);
    
    // Assert no exceptions
  }

  @Test(expected = IllegalArgumentException.class)
  public void testUserInputInvalid() {
    IController controller = new MockController();
    controller.userInput(-1, -1); 
  }

}


