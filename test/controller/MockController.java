package controller;

class MockController implements IController {

    @Override
    public void nextFrame() {
      // Mock implementation
    }
  
    @Override
    public void userInput(int x, int y) {
      // Mock implementation
    }
  
  }