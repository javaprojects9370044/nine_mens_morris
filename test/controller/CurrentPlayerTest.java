package controller;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.Test;

public class CurrentPlayerTest {

  @Test
  public void testEnumValues() {
    Current_Player[] expected = {Current_Player.WHITE_PLAYER, Current_Player.BLACK_PLAYER};
    Current_Player[] actual = Current_Player.values();
    assertArrayEquals(expected, actual);
  }

  @Test 
  public void testOrdinal() {
    assertEquals(0, Current_Player.WHITE_PLAYER.ordinal());
    assertEquals(1, Current_Player.BLACK_PLAYER.ordinal());
  }

}





