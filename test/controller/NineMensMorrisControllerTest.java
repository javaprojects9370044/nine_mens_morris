package controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.Test;

import java.util.ArrayList;

import model.Color;
import model.Game;
import model.Player;
import model.Simple_Result;

public class NineMensMorrisControllerTest {

  @Test
  public void testCurrentUsedPieces_blackPlayer() {
    Game game = new Game();
    Player blackPlayer = new Player(Color.BLACK);
    game.setCurrentPlayer(blackPlayer);
    
    Nine_mens_morris_controller controller = new Nine_mens_morris_controller();
    controller.set_model(game);
    
    ArrayList<Simple_Result<Integer, Integer>> pieces = controller.current_used_Pieces();
    
    // Validate pieces list only contains black player's used positions
    for(Simple_Result<Integer, Integer> piece : pieces) {
      assertEquals(0, (int)piece.value()); 
    }
  }

  @Test
  public void testCurrentUsedPieces_emptyBoard() {
    Game game = new Game();
    
    Nine_mens_morris_controller controller = new Nine_mens_morris_controller();
    controller.set_model(game);
    
    ArrayList<Simple_Result<Integer, Integer>> pieces = controller.current_used_Pieces();
    
    assertTrue(pieces.isEmpty()); 
  }
  @Test
  public void testCurrentUsedPieces_bothPlayers() {
    Game game = new Game();
    Player blackPlayer = new Player(Color.BLACK);
    Player whitePlayer = new Player(Color.WHITE);
    game.setCurrentPlayer(blackPlayer);
    game.setOpponentPlayer(whitePlayer);
    
    // Add used pieces for both players
    blackPlayer.getBoard().setUsed(1, true);
    blackPlayer.getBoard().setUsed(2, true);
    whitePlayer.getBoard().setUsed(3, true);
    
    Nine_mens_morris_controller controller = new Nine_mens_morris_controller();
    controller.set_model(game);
    
    ArrayList<Simple_Result<Integer, Integer>> pieces = controller.current_used_Pieces();

    // Validate pieces for both players
    assertEquals(3, pieces.size());
    assertEquals(0, (int)pieces.get(0).value()); 
    assertEquals(0, (int)pieces.get(1).value());
    assertEquals(255, (int)pieces.get(2).value());
  }
  @Test
public void testCurrentUsedPieces_invalidBoardPosition() {
  Game game = new Game();
  Player blackPlayer = new Player(Color.BLACK);
  game.setCurrentPlayer(blackPlayer);

  // Set invalid board position
  blackPlayer.getBoard().setUsed(-1, true);
  
  Nine_mens_morris_controller controller = new Nine_mens_morris_controller();
  controller.set_model(game);

  ArrayList<Simple_Result<Integer, Integer>> pieces = controller.current_used_Pieces();

  assertTrue(pieces.isEmpty());
}

@Test 
public void testCurrentUsedPieces_nullGame() {
  Nine_mens_morris_controller controller = new Nine_mens_morris_controller();
  
  ArrayList<Simple_Result<Integer, Integer>> pieces = controller.current_used_Pieces();

  assertTrue(pieces.isEmpty());
}

@Test
public void testCurrentUsedPieces_nullPlayer() {
  Game game = new Game();
  game.setCurrentPlayer(null);

  Nine_mens_morris_controller controller = new Nine_mens_morris_controller();
  controller.set_model(game);

  ArrayList<Simple_Result<Integer, Integer>> pieces = controller.current_used_Pieces();

  assertTrue(pieces.isEmpty());
}

}
