package controller;

/**
 * Enumeration of the current player in the game.
 * WHITE_PLAYER and BLACK_PLAYER represent the two players.
 */
public enum Current_Player {
    WHITE_PLAYER, BLACK_PLAYER;
}