package controller;

/**
 * The controller interface from the perspective of a view object. 
 * Any parts that are not part of this interface are not visible from the view object.
 */
public interface IController {
    /**
     * Process that generates and draws the next picture depending on the state of the game and the game changes within that state.
     */
    public void nextFrame();
    /** 
     * Process that handles user input.
     * @param mouse_coordinate_x the x coordinate of the mouse click
     * @param mouse_coordinate_y the y coordinate of the mouse click
    */
    public void userInput(int mouse_coordinate_x, int mouse_y);
}