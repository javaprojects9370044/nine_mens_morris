package controller;
import view.IView;

import java.util.ArrayList;
import java.util.stream.IntStream;

import model.*;
/**
 * The controller class will handle the logic of the game. It will take as input
 * the current game state and the user input, and then update the game state.
 * @param view The view object that will display the game.
 * @param game_state The current game state.
 * @param game_model The model object that will store the game state.
 * @param marked_piece The board position of the piece that is being marked.
 */
public class Nine_mens_morris_controller implements IController {
    private Game_State game_state;
    private IView view;
    private IModel game_model;
    private int marked_piece;

    /**
     * Gets a list of SimpleResult objects representing the used pieces on the board
     * for the current player and opponent.
     * Loops through 0-24 board positions and checks if each position is used for
     * the current player and opponent based on their color.
     * Adds SimpleResult objects to the list with the board position and color
     * value.
     * @return A list of SimpleResult objects representing the used pieces on the board
     */
    private ArrayList<Simple_Result<Integer, Integer>> current_used_Pieces() {
        ArrayList<Simple_Result<Integer, Integer>> used_pieces = new ArrayList<>();
        if (game_model.current_Player().color() == Color.BLACK) {
            IntStream.range(0, 24)
                    .filter(i -> game_model.current_Player().board().is_used(i))
                    .forEach(i -> used_pieces.add(new Simple_Result<>(i, 0)));
        } else if (game_model.current_Player().color() == Color.WHITE) {
            IntStream.range(0, 24)
                    .filter(i -> game_model.current_Player().board().is_used(i))
                    .forEach(i -> used_pieces.add(new Simple_Result<>(i, 255)));
        }
        if (game_model.opponent_player().color() == Color.BLACK) {
            IntStream.range(0, 24)
                    .filter(i -> game_model.opponent_player().board().is_used(i))
                    .forEach(i -> used_pieces.add(new Simple_Result<>(i, 0)));
        } else if (game_model.opponent_player().color() == Color.WHITE) {
            IntStream.range(0, 24)
                    .filter(i -> game_model.opponent_player().board().is_used(i))
                    .forEach(i -> used_pieces.add(new Simple_Result<>(i, 255)));
        }

        return used_pieces;
    }

    /**
     * Constructor for Nine_mens_morris_controller.
     * Initializes the game state to TITLE_SCREEN.
     * @return Nine_mens_morris_controller object
     */
    public Nine_mens_morris_controller() {
        this.game_state = Game_State.TITLE_SCREEN;
    }

    /**
     * Sets the view that this controller will use to display the game.
     * 
     * @param view The view instance that will be used by this controller.
     */
    public void set_view(IView view) {
        this.view = view;
    }

    /**
     * Sets the game model that this controller will use.
     * 
     * @param game_model The game model instance that will be used by this
     *                   controller.
     */
    public void set_model(IModel game_model) {
        this.game_model = game_model;
    }

    /**
     * Updates the view each frame based on the current game state.
     * 
     * Switches on the game_state to determine which view method to call.
     * The view methods will draw the appropriate screens and messages.
     * 
     * Before switching, it calls view.update_tokens() to update the token
     * positions.
     */
    @Override
    public void nextFrame() {
        view.update_tokens(current_used_Pieces());
        switch (game_state) {
            case TITLE_SCREEN -> {
                this.view.drawTitleScreen();
            }
            case GAME_WON -> {
                this.view.draw_won("Hurray, you've Won!");
            }
            case GAME_SCREEN -> {
                this.view.drawGame();
            }
            case IMPOSSIBLE_MOVE -> {
                this.view.draw_impossible(((game_model.current_Player().color() == Color.BLACK) ? "Black" : "White")
                        + "Impossible,try again");
            }
            case REMOVE_OPPONENT_PIECE -> {
                this.view.drawGame();
                this.view.draw_remove_opponent_piece(
                        ((game_model.opponent_player().color() == Color.BLACK) ? "Black" : "White")
                                + "Remove opponent's piece");
            }
            case MARKING_PIECE -> {
                this.view.drawGame();
                this.view.draw_jumping_or_moving(
                        ((game_model.current_Player().color() == Color.BLACK) ? "Black" : "White")
                                + "Please mark piece to move");
            }
            case JUMPING_OR_MOVING -> {
                this.view.drawGame();
                this.view.draw_jumping_or_moving(
                        ((game_model.current_Player().color() == Color.BLACK) ? "Black" : "White")
                                + "Please mark place to jump to");
            }
        }
    }

    /**
     * Updates the game state based on the current player's state.
     * 
     * If the current player is in the Moving or EndGame phase,
     * set the game state to MARKING_PIECE to allow the player to mark a piece to
     * move.
     * 
     * Otherwise, set the game state to GAME_SCREEN to show the normal game screen.
     */
    private void player_dependent_status_change() {
        this.game_state = (this.game_model.current_Player().state() == model.GamePhase.Moving ||
                this.game_model.current_Player().state() == model.GamePhase.EndGame) ? Game_State.MARKING_PIECE
                        : Game_State.GAME_SCREEN;
    }
    @Override
    public void userInput(int mouse_x, int mouse_y) {
        Integer token_id = this.view.find_token_position(mouse_x, mouse_y);
        switch (game_state) {
            case TITLE_SCREEN:
                this.game_state = Game_State.GAME_SCREEN;
                break;
            case GAME_WON:
                this.game_state = Game_State.TITLE_SCREEN;
                break;
            case IMPOSSIBLE_MOVE:
                this.game_state = Game_State.GAME_SCREEN;
                break;
            default:
                if(token_id== -1) {
                    break;
                }  else if (game_state == Game_State.MARKING_PIECE) {
                    this.marked_piece = token_id;
                    this.game_state = Game_State.JUMPING_OR_MOVING;
                    break;
                } else if (game_state == Game_State.REMOVE_OPPONENT_PIECE) {
                    Simple_Result<Game, Move> current=this.game_model.remove_opponent_piece(token_id);
                    if(current.move()==Move.POSSIBLE_MOVE){
                        set_model(current.game());
                        this.game_state= (current.game().current_Player().state()!=model.GamePhase.Placing)? 
                        (game_model.checkWinCondition().status()==GameStatus.BlackWon||game_model.checkWinCondition().status()==GameStatus.WhiteWon)? 
                        Game_State.GAME_WON : Game_State.MARKING_PIECE : Game_State.GAME_SCREEN;
                        break;
                    } else{
                        break;
                    }
                } else {
                    handle_player_input(token_id);
                    break;
                }
        }
    }

    /**
     * Handles player input based on the current game state and player.
     * Checks the current player's state (Placing, Moving, EndGame) and calls the
     * appropriate handler method.
     * 
     * @param token_id The id of the token that was clicked.
     */
    private void handle_player_input(int token_id) {
        System.out.println(token_id);
        switch (game_model.current_Player().state()) {
            case Placing:
                System.out.println("Placing");
                handlePlacing(token_id, game_model.current_Player().color());
                break;
            case Moving:
                System.out.println("Moving");
                handleMovingOrJumping(token_id, marked_piece, game_model.current_Player().color());
                break;
            case EndGame:
                System.out.println("EndGame");
                handleMovingOrJumping(token_id, marked_piece, game_model.current_Player().color());
                break;
        }
    }
    
    /**
     * Handles placing a piece for the current player.
     * Checks if the move is valid and updates the game state accordingly.
     * 
     * @param token_id The position to place the piece.
     * @param color    The color of the current player.
     */
    private void handlePlacing(Integer token_id, Color color) {
        Simple_Result<Game, Move> current = game_model.place_piece(token_id);
        switch (current.move()) {
            case IMPOSSIBLE_MOVE:
                System.out.println("Impossible Move");
                this.game_state = Game_State.IMPOSSIBLE_MOVE;
                set_model(current.game());
                break;
            case POSSIBLE_MOVE:
                System.out.println("Possible Move");
                set_model(current.game());
                player_dependent_status_change();
                break;
            case POSSIBLE_MOVE_REMOVE_OPPONENT_PIECE:
                System.out.println("Possible Move Remove Opponent Piece");
                set_model(current.game());
                this.game_state = Game_State.REMOVE_OPPONENT_PIECE;
                break;
        }
    }
    
    /**
     * Handles moving or jumping a piece for the current player.
     * Checks if the move is valid and updates the game state accordingly.
     * @param new_position The new position of the piece.
     * @param old_position The old position of the piece.
     */
    private void handleMovingOrJumping(Integer new_position, Integer old_position, Color color) {
        Simple_Result<Game, Move> current = game_model.move_or_jump(new_position, old_position);
        switch (current.move()) {
            case IMPOSSIBLE_MOVE:
                this.game_state = Game_State.MARKING_PIECE;
                set_model(current.game());
                break;
            case POSSIBLE_MOVE:
                set_model(current.game());
                player_dependent_status_change();
                break;
            case POSSIBLE_MOVE_REMOVE_OPPONENT_PIECE:
                set_model(current.game());
                this.game_state = Game_State.REMOVE_OPPONENT_PIECE;
                break;
        }
    }
    
}