/**
 * The controller package contains classes for handling user input and game state 
 * changes.
 * 
 * The main class is Nine_mens_morris_controller which implements the IController 
 * interface. It has fields for tracking the current game state, player turn, and 
 * view. It has methods for setting the view, handling user input, and updating the 
 * game state each frame.
 * 
 * Game_State is an enum representing the possible screens of the game - title screen, 
 * game screen, and win screens.
 * 
 * Current_Player is an enum for whose turn it is - WHITE_PLAYER or BLACK_PLAYER.
 * 
 * The IController interface defines the core methods the controller needs - nextFrame() 
 * for updating each frame, and userInput() for handling mouse clicks.
 * 
 * So in summary, the controller classes take input, update the game state, and pass 
 * information to the view to display. Nine_mens_morris_controller brings it together 
 * as the main controller, using the Game_State and Current_Player enums to track 
 * progress. IController defines the essential interface for the view to interact 
 * with the controller.
 */
package controller;