package controller;
/**
 * Game_State is an enum that represents the different states of the game.
 * 
 * TITLE_SCREEN: The initial title screen shown when the game starts.
 * GAME_SCREEN: The main game screen where players take turns. 
 * GAME_WON_WHITE: The state when the white player has won.
 * GAME_WON_BLACK: The state when the black player has won.
 * IMPOSSIBLE_MOVE: The state when an invalid move was attempted.
 * REMOVE_OPPONENT_PIECE: The state when removing an opponent's piece. 
 * JUMPING_OR_MOVING: The state when jumping over or moving a piece.
 */
public enum Game_State {

    TITLE_SCREEN,
    GAME_SCREEN,
    GAME_WON,
    IMPOSSIBLE_MOVE,
    REMOVE_OPPONENT_PIECE,
    JUMPING_OR_MOVING,
    MARKING_PIECE;
}