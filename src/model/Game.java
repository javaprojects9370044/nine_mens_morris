package model;

/**
 * The Game class represents the state of a game of Nine Men's Morris.
 * It contains the current status, players, and methods to make moves,
 * check win conditions, run AI, and more.
 * @param status contains the current status, in which the Game is.@interface
 * @param current_player This is the player who can now make a move.
 * @param opponent_player thats the opponent player
 * @interface IModel This is the interface through which the controller communicates with the model.
 */
public record Game(GameStatus status, Player current_Player, Player opponent_player) implements IModel {
    public static Game of() {
        return new Game(
                GameStatus.InProgress,
                new Player(Color.BLACK, GamePhase.Placing, MillBoard.of(), 0),
                new Player(Color.WHITE, GamePhase.Placing, MillBoard.of(), 0));
    }

    /**
     * Checks if the game has been won and updates the game status accordingly.
     * Returns a new Game instance with the updated status.
     */
    @Override
    public Game checkWinCondition() {
        return new Game(
                        (
                            this.opponent_player().board().deadZone() && this.opponent_player().color() == Color.WHITE && 
                            this.opponent_player().state()==GamePhase.EndGame ||this.current_Player().board().deadZone() && 
                            this.current_Player().color()== Color.WHITE && this.current_Player().state()==GamePhase.EndGame)?
                            GameStatus.BlackWon :
                            (this.current_Player().board().deadZone() && current_Player().color() == Color.BLACK && 
                            this.current_Player().state()==GamePhase.EndGame ||this.opponent_player().board().deadZone() && 
                            opponent_player().color() == Color.BLACK && this.opponent_player().state()==GamePhase.EndGame)?
                            GameStatus.WhiteWon: this.status, current_Player, opponent_player);
    }

    /**
     * Checks if the game is over by calling checkWinCondition()
     * and seeing if the status is BlackWon or WhiteWon.
     * 
     * @return True if game is over, false otherwise.
     */
    boolean is_game_over() {
        return (this.checkWinCondition().status() == GameStatus.BlackWon ||
                this.checkWinCondition().status() == GameStatus.WhiteWon);
    }

    @Override
    public Simple_Result<Game, Move> makeAIMove() {
        return Game_Artificial_Intelligence.game_ai_start(this).make_best_move();
    }

    /**
     * Overrides the move_or_jump method from the IModel interface.
     * Allows a player to move or jump one of their pieces to a new position on the
     * board.
     * 
     * @param new_position The new position to move/jump the piece to.
     * @param old_position The original position of the piece being moved/jumped.
     * @return A SimpleResult containing the updated Game state after the move, and
     *         the Move made.
     */
    @Override
    public Simple_Result<Game, Move> move_or_jump(int new_position, int old_position) {
        return Move.move_ore_jump_execution(this, new_position, old_position);
    }

    /**
     * Overrides the remove_opponent_piece method from the IModel interface.
     * Allows a player to remove one of the opponent's pieces from the
     * board at the given position.
     *
     * @param position The position of the opponent's piece to remove.
     * @return A SimpleResult containing the updated Game state after removing
     *         the piece, and the Move made.
     */
    @Override
    public Simple_Result<Game, Move> remove_opponent_piece(int position) {
        return Move.piece_removal_execution(this, position);
    }

    /**
     * Overrides the place_piece method from the IModel interface.
     * Allows a player to place one of their pieces at the given position on the
     * board.
     * 
     * @param position The position to place the piece.
     * @return A SimpleResult containing the updated Game state after placing the
     *         piece,
     *         and the Move made.
     */
    @Override
    public Simple_Result<Game, Move> place_piece(int position) {
        return Move.piece_place_execution(this, position);
    }

}