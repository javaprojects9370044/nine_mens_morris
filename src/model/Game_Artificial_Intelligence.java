package model;

import java.util.Comparator;
import java.util.Random;

import java.util.ArrayList;

/**
 * Game_Artificial_Intelligence implements AI players for the game of Nine Men's Morris. 
 * It contains methods to generate random legal moves, find the best move by rating 
 * all possible moves, and rate individual moves by analyzing the resulting game state.
 * @param current_game The current game state object
 * @param rand         The random number generator to use for generating random
 *                     moves.
 */
public record Game_Artificial_Intelligence(Game current_game, Random rand) {
        /**
         * Creates a new Game_Artificial_Intelligence instance to play the given Game.
         * 
         * @param current_game The Game instance for the AI to play.
         * @return A new Game_Artificial_Intelligence instance to play the given Game.
         */
        static Game_Artificial_Intelligence game_ai_start(Game current_game) {
                return new Game_Artificial_Intelligence(current_game, new Random());
        }

        /**
         * Generates a random legal move for the current game state by:
         * 1. Generating all possible moves with Move.generate_possible_moves()
         * 2. Removing illegal moves with Move.remove_possible_pieces()
         * 3. Skipping a random number of moves with rand.nextInt()
         * 4. Returning the first remaining move
         * 
         * @return A random legal move for the current game state
         */
        Simple_Result<Game, Move> make_random_move() {
                ArrayList<Simple_Result<Game, Move>> cleanedUdo = Move
                                .remove_possible_pieces(Move.generate_possible_moves(current_game));
                return cleanedUdo.stream()
                                .skip(this.rand.nextInt(cleanedUdo.size()))
                                .findFirst()
                                .get();
        }

        /**
         * Generates all possible moves for the current game state,
         * removes illegal moves, rates each move by calling rate_move(),
         * and returns the move with the maximum rating, which is
         * considered the "best" move.
         * 
         * @return A Simple_Result object, containing a game state
         */
        Simple_Result<Game, Move> make_best_move() {
                ArrayList<Simple_Result<Game, Move>> cleanedUdo = Move
                                .remove_possible_pieces(Move.generate_possible_moves(current_game));
                return cleanedUdo.stream()
                                .max(Comparator.comparing(this::rate_move))
                                .get();
        }

        /**
         * Rates a given move by evaluating the resulting game state.
         * 
         * @param to_evaluate_move The move to evaluate
         * @return An integer rating for the move:
         *         - 0 for an impossible move
         *         - 1 for a possible move
         *         - 2 for a regular move
         *         - 3 for a winning move in endgame
         *         - 4 for a losing move in endgame
         */
        Integer rate_move(Simple_Result<Game, Move> to_evaluate_move) {
                return (to_evaluate_move.move() == Move.IMPOSSIBLE_MOVE) ? 0
                                : (to_evaluate_move.move() == Move.POSSIBLE_MOVE) ? 1
                                : (to_evaluate_move.game().current_Player()
                                        .state() == GamePhase.EndGame)
                                ? (to_evaluate_move.game()
                                .status() == GameStatus.BlackWon
                                || to_evaluate_move.game()
                                .status() == GameStatus.WhiteWon)? 4: 3: 2;
        }

}