package model;
/**  
 * The model package contains classes that represent the game state and 
 * logic.
 * 
 * The main interface is IModel, which defines methods like making moves, 
 * placing pieces, checking win conditions, etc.
 * 
 * Game is the main class that implements IModel and holds the current game 
 * status, players, and other state. It has logic for making AI moves, 
 * checking win conditions, etc.
 * 
 * GameStatus is an enum representing the possible states of a game - in 
 * progress, black won, or white won.
 * 
 * GamePhase is an enum for the different phases of the game - placing 
 * pieces, moving pieces, end game.
 * 
 * Color is an enum for the two player colors - BLACK and WHITE.
 * 
 * MillBoard is a record that contains the board state as a BitSet. It has 
 * methods for modifying the board by setting piece positions.
 * 
 * SimpleResult is a generic record that holds a game state and move, 
 * used for return values.
 * So in summary, the model classes encapsulate all the game data and logic 
 * needed for playing the game. The main Game class brings it all together 
 * to represent the full state of a game. IModel defines the key game 
 * methods that the rest of the app uses.
*/