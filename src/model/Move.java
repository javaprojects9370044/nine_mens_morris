package model;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
/** 
* The move class will check if the input is a valid move and then 
* execute it. It takes as input the current state of the game, 
* makes the move, produces a new game and returns it.
* 
*/
public enum Move {
    IMPOSSIBLE_MOVE,
    POSSIBLE_MOVE,
    POSSIBLE_MOVE_REMOVE_OPPONENT_PIECE;

    /**
     * Wraps a board position index to keep it within the bounds of the board.
     * 
     * @param position The position index to wrap
     * @return The wrapped index between 0-23
     */
    public static int board_wrapper(int position) {
        return (position + 24) % 24;
    }
    /**
     * Checks if placing a piece at the given position is a valid move in the
     * current game state.
     * 
     * @param current_game The current game state object
     * @param new_pos      The position to check for a valid piece placement
     * @return IMPOSSIBLE_MOVE if the game is over or position is invalid,
     *         POSSIBLE_MOVE if the placement is valid,
     *         POSSIBLE_MOVE_REMOVE_OPPONENT_PIECE if the placement results in a
     *         mill
     */
    private static Move is_valid_placing(Game current_game, int new_pos) {
        return (
            (current_game.status() == GameStatus.BlackWon) || 
            (current_game.status() == GameStatus.WhiteWon) ||  
                (current_game.current_Player().board().is_used(new_pos) || 
                current_game.opponent_player().board().is_used(new_pos))
                )? IMPOSSIBLE_MOVE
                : POSSIBLE_MOVE;
    }

    /**
     * Checks if moving a piece from old_pos to new_pos is a valid jump move in the
     * current game state.
     * 
     * @param current_game The current game state object
     * @param new_pos      The new position to move the piece to
     * @param old_pos      The old position to move the piece from
     * @return IMPOSSIBLE_MOVE if the jump is invalid, POSSIBLE_MOVE if it is valid
     */
    private static Move is_valid_jump(Game current_game, int new_pos, int old_pos) {
        return (new_pos==old_pos) ? IMPOSSIBLE_MOVE
                        : is_valid_placing(current_game, new_pos);
    }

    /**
     * Checks if moving a piece from old_pos to new_pos is a valid move in the
     * current game state.
     * @param current_game The current game state object
     * @param new_pos      The new position to move the piece to
     * @param old_pos      The old position to move the piece from
     * @return IMPOSSIBLE_MOVE if the Move is invalid, POSSIBLE_MOVE if it is valid
     */
    private static Move is_valid_move(Game current_game, int new_pos, int old_pos) {
        return (
            is_valid_jump(current_game, new_pos, old_pos) == IMPOSSIBLE_MOVE || 
                ((current_game.current_Player().board().is_in_corner(board_wrapper(old_pos))) && 
                current_game.current_Player().board().is_in_corner(board_wrapper(new_pos)))
                ) ? IMPOSSIBLE_MOVE
                : (board_wrapper(old_pos-new_pos) % 3 == 0 && board_wrapper(old_pos-new_pos) % 6!= 0 ||
                board_wrapper(new_pos-old_pos) % 3 == 0 && board_wrapper(new_pos-old_pos) % 6!= 0 ||
                        ((old_pos-new_pos)== 1) ||
                        ((new_pos-old_pos)== 1))
                                ? POSSIBLE_MOVE
                                : IMPOSSIBLE_MOVE;
    }
    /**
     * Executes a piece placement move in the given game state.
     * Checks if placing a piece at the given position is valid.
     * If valid, returns a new game state with the piece placed.
     * If invalid, returns the original game state.
     * 
     * @param current_game The current game state object
     * @param position     The position on the board to check for piece removal
     * @return A result with information on how successful a piece placement was.
     */
    // 
    static Simple_Result<Game, Move> piece_place_execution(Game current_game, int position) {
        return (
                (Move.is_valid_placing(current_game, position) == IMPOSSIBLE_MOVE) ||
                current_game.current_Player().state()!= GamePhase.Placing)?
                        new Simple_Result<Game, Move>(current_game, IMPOSSIBLE_MOVE) :
                        new Simple_Result<Game, Move>(
                                new Game(
                                        current_game.status(),
                                        current_game.opponent_player(),
                                        current_game.current_Player().setPiece(position)),
                                        current_game.current_Player().board().isInMill(position)? 
                                        POSSIBLE_MOVE_REMOVE_OPPONENT_PIECE : POSSIBLE_MOVE);
    }

    /**
     * Executes a piece removal move in the given game state.
     * Checks if removing the piece at the given position is valid.
     * If valid, returns a new game state with the piece removed.
     * If invalid, returns the original game state.
     * @param current_game The current game state object
     * @param position     The position on the board to check for piece removal
     * @return A result with information on how successful a piece placement was.
     */
    static Simple_Result<Game, Move> piece_removal_execution(Game current_game, int position) {
            List<Integer> existing_pieces = IntStream.range(0, 23)
                            .filter(i -> current_game.current_Player().board().is_used(i))
                            .boxed()
                            .collect(Collectors.toList());
            List<Integer> mill_positions = existing_pieces.stream()
                            .filter(i -> current_game.current_Player().board().isInMill(i))
                            .collect(Collectors.toList());
            // Check if removing this piece would break up an existing mill
            boolean removesMillPiece = mill_positions.contains(position);
            // Only allow removing pieces that are not in a mill,
            // or if all pieces are in mills
            return (current_game.current_Player().board().is_used(position) &&
                            !(removesMillPiece && mill_positions.size() < existing_pieces.size()))
                                            ? new Simple_Result<Game, Move>(
                                                            new Game(
                                                                            current_game.checkWinCondition()
                                                                                            .status(),
                                                                            current_game.current_Player()
                                                                                            .removePiece(position),
                                                                            current_game.opponent_player()),
                                                            POSSIBLE_MOVE)
                                            : new Simple_Result<Game, Move>(
                                                            current_game,
                                                            IMPOSSIBLE_MOVE);
    }


    /**
     * Executes a move or jump action in the given game state.
     * Checks if moving/jumping the piece from old_position to new_position is valid
     * based on the current game phase.
     * If valid, returns a new game state with the piece moved/jumped.
     * If invalid, returns the original game state.
     * @param current_game The current game state object
     * @param position     The position on the board to check for piece removal
     * @return A result with information on how successful a piece placement was.
     */
    static Simple_Result<Game, Move> move_ore_jump_execution(
        Game current_game, int new_position, int old_position) {
        return (current_game.current_Player().state() == GamePhase.Moving)? 
                (Move.is_valid_move(current_game, new_position, old_position) == Move.IMPOSSIBLE_MOVE? 
                new Simple_Result<Game, Move>(current_game, Move.IMPOSSIBLE_MOVE)
                                : new Simple_Result<Game, Move>(
                                        new Game(
                                                current_game.status(),
                                                current_game.opponent_player(),
                                                current_game.current_Player().movePiece(new_position, old_position)),
                                                (current_game.current_Player().board()
                                                .isInMill(new_position)&& new_position != old_position)? 
                                                Move.POSSIBLE_MOVE_REMOVE_OPPONENT_PIECE : Move.POSSIBLE_MOVE))
                        : (current_game.current_Player().state() == GamePhase.EndGame)? 
                        (Move.is_valid_jump(current_game, new_position, old_position) == Move.IMPOSSIBLE_MOVE
                                        ? new Simple_Result<Game, Move>(current_game, Move.IMPOSSIBLE_MOVE)
                                        : new Simple_Result<Game, Move>(
                                                new Game(
                                                        current_game.status(),
                                                        current_game.opponent_player(),
                                                        current_game.current_Player().movePiece(new_position,old_position)),
                                                        (current_game.current_Player().board()
                                                        .isInMill(new_position)&& new_position != old_position)? 
                                                        Move.POSSIBLE_MOVE_REMOVE_OPPONENT_PIECE : Move.POSSIBLE_MOVE))
                                : new Simple_Result<Game, Move>(current_game, Move.IMPOSSIBLE_MOVE);
    }

    /**
     * Generates all possible moves for the current player in the given game state.
     * Checks for valid piece placement, movement, jumping based on game phase.
     * @param current_game The current game state object
     * @return list of possible game states with valid moves applied.
     */
    static ArrayList<Simple_Result<Game, Move>> generate_possible_moves(Game current_game) {
        ArrayList<Simple_Result<Game, Move>> possible_moves = new ArrayList<>();
        switch (current_game.current_Player().state()) {
            case Placing:
                IntStream.range(0, 24).filter(i -> piece_place_execution(current_game, i).move() != IMPOSSIBLE_MOVE)
                        .forEach(i -> possible_moves.add(piece_place_execution(current_game, i)));
                break;

            default:
                IntStream.range(0, 24).boxed()
                        .flatMap(i -> IntStream.range(0, 24).mapToObj(j -> move_ore_jump_execution(current_game, i, j)))
                        .filter(result -> result.move() != IMPOSSIBLE_MOVE)
                        .forEach(result -> possible_moves.add(result));
        }
        return possible_moves;
    }

    /**
     * Removes possible piece removal moves from a list of generated moves.
     * 
     * Iterates through the generated moves, checking if each move is a piece
     * removal.
     * If so, generates all possible piece removal moves on the current board and
     * adds them to a new list.
     * Other moves are added directly to the new list.
     * @param generated_moves The list of generated moves
     * @return the new list with piece removal moves expanded.
     */
    static ArrayList<Simple_Result<Game, Move>> remove_possible_pieces(
            ArrayList<Simple_Result<Game, Move>> generated_moves) {
        ArrayList<Simple_Result<Game, Move>> possible_moves = new ArrayList<>();
        generated_moves.parallelStream().forEach(generated_move -> {
            if (generated_move.move() == POSSIBLE_MOVE_REMOVE_OPPONENT_PIECE) {
                IntStream.range(0, 24)
                        .parallel()
                        .filter(i -> piece_removal_execution(generated_move.game(), i).move() == POSSIBLE_MOVE)
                        .mapToObj(i -> piece_removal_execution(generated_move.game(), i))
                        .forEach(possible_moves::add);
            } else {
                possible_moves.add(generated_move);
            }
        });
        return possible_moves;
    }



}


