/**
 * IModel interface defines the API for the game model. 
 * It contains methods to make moves, check game status, 
 * get current player, etc. This is the main interface 
 * for interacting with the game model.
 */
package model;

public interface IModel {

  /**
   * Performs a move or jump with the current player's piece.
   * 
   * @param new_position New position to move/jump to
   * @param old_position Current position of piece
   * @return A Simple_Result containing the updated Game state after the move,
   *         and the Move made
   */
  Simple_Result<Game, Move> move_or_jump(int new_position, int old_position);

  /**
   * Removes an opponent's piece from the given position on the board.
   * 
   * @param position The position on the board to remove the piece from.
   * @return A Simple_Result containing the updated Game state after removing
   */
  Simple_Result<Game, Move> remove_opponent_piece(int position);

  /**
   * Places a piece for the current player at the given board position.
   * 
   * @param position The position on the board to place the piece.
   * @return A Simple_Result containing the updated Game state after placing the
   *         piece,
   *         and the Move made
   */
  Simple_Result<Game, Move> place_piece(int position);

  /**
   * Performs a move by the AI player.
   * 
   * @return A Simple_Result containing the updated Game state after the AI move,
   *         and the Move made by the AI player.
   */
  Simple_Result<Game, Move> makeAIMove();

  /**
   * Checks if there is a winner in the current game state.
   *
   * @return The Game object with the winner set if there is one, otherwise
   *         unchanged.
   */
  Game checkWinCondition();

  /**
   * Gets the current status of the game.
   *
   * @return The GameStatus enum representing if the game is ongoing, ended in a
   *         win, draw, etc.
   */
  GameStatus status();

  /**
   * Gets the Player whose turn it currently is.
   * @return The Player whose turn it currently is.
   */
  Player current_Player();

  /**
   * Gets the Player that is the opponent of the current Player.
   *
   * @return The opponent Player.
   */
  Player opponent_player();
}