package model;
import java.util.Arrays;
import java.util.BitSet;

/** 
 * The MillBoard class represents the game board and contains methods to
 * interact with it:
 * - setBoard() sets a position on the board to a given boolean value
 * - checkPossibleMills() checks if any mills exist on the current board state
 * - is_used() checks if a given position on the board is used
 * - dangerZone() checks if there are less than 4 pieces left
 * - deadZone() checks if there are less than 3 pieces left
 * - isInMill() checks if a given position is part of a mill
 * - checkMillCondition() helper method to check mill conditions
 * @param The board parameter represents the bitset that stores the state of the board.
 */
public record MillBoard(BitSet board) {
  /**
   * Creates a new MillBoard instance with an empty board (BitSet of size 24).
   * This serves as a factory method to construct a MillBoard.
   * @return A new MillBoard instance with an initialized BitSet.
   */
  static MillBoard of() {
    return new MillBoard(new BitSet());
  }
  /**
   * Sets the value on the board at the given position.
   *
   * @param pos   The position on the board to set.
   * @param value The boolean value to set at the position.
   * @return A new MillBoard instance with the updated board state.
   */
  MillBoard setBoard(int pos, boolean value) {
    BitSet cur = board();
    cur.set(Move.board_wrapper(pos), value);
    return new MillBoard(cur);
  }

  /**
   * Checks if there are any mills (3 in a row) on the current board state.
   * Iterates through a predefined set of positions to check if they are in a
   * mill.
   * @return true if any position is in a mill, false otherwise.
   */
  boolean checkPossibleMills() {
    return Arrays.stream(new int[] { 1, 2, 3, 9, 13, 14, 15, 21 })
        .anyMatch(number -> isInMill(number));
  }

  /**
   * Checks the boolean value of the board at the given position.
   * 
   * @param pos
   * @return True if the position is already claimed, false otherwise.
   */
  public boolean is_used(int pos) {
    return board().get((pos+24)%24);
  }

  /**
   * Checks if there are less than 4 pieces left on the board, indicating the end
   * game "danger zone".
   * 
   * @return True if there are less than 4 pieces left, false otherwise.
   */
  public boolean dangerZone() {
    return board.cardinality() < 4;
  }

  /**
   * Checks if there are less than 3 pieces left on the board,
   * indicating the end game "danger zone".
   * 
   * @return True if there are less than 3 pieces left, false otherwise.
   */
  boolean deadZone() {
    return board.cardinality() < 3;
  }

  /**
   * Checks if the given board position is part of a mill (3 in a row).
   * 
   * This is done by checking the 3 positions offset before and after the given
   * position, as well as the 2 diagonal positions, to see if they are occupied.
   * 
   * @param position The board position to check
   * @return True if the position is part of a mill, false otherwise
   */
  boolean isInMill(int position) {
    int remainder = position % 3;
    return (
      (remainder == 0 && is_used(position + 1) && is_used(position + 2) && !is_in_corner(position+1) && !is_in_corner(position+2)) ||
      (remainder == 2 && is_used(position - 1) && is_used(position - 2) && !is_in_corner(position-1) && !is_in_corner(position-2)) ||
      (remainder == 1 && is_used(position - 1) && is_used(position + 1) && !is_in_corner(position-1) && !is_in_corner(position+1))||
      (position % 6 == remainder && is_used(position + 3) && is_used(position + 6)) ||
      (position % 6 == remainder && is_used(position - 3) && is_used(position - 6))||
      (is_used(position+3) && is_used(position-3) && !is_in_corner(position))
      ) ? true : false;
  }

  /**
   * Checks if the given board position is a corner piece.
   * 
   * Corner pieces are at indices 0, 1, 2, 6, 7, 8, 12, 13, 14, 18, 19, 20.
   * 
   * @param position The board position to check
   * @return True if the position is a corner piece, false otherwise
   */
  boolean is_in_corner(int position) {
    int[] corner_pieces = { 0, 1, 2, 6, 7, 8, 12, 13, 14, 18, 19, 20 };
    return (Arrays.stream(corner_pieces).anyMatch(number -> number == position)) ? true : false;
  }
}
