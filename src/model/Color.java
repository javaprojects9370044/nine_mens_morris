package model;
/**
 * The color enum class will be used to represent the color of a Player.
 */
public enum Color {
  BLACK,
  WHITE;
}