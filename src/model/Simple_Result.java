package model;
/**
 * This is a container record containing a game and a move class score. If a move 
 * was possible, don't worry. It's already done. If the move was impossible, you'll 
 * have to try again. If the move was possible, and you're allowed to remove the 
 * opponent's checker, you'll be shown the current state of the game, where your 
 * checker is placed, and you'll be asked to remove an opponent's checker.
 */
public record Simple_Result<T,G>(T game, G move) {}