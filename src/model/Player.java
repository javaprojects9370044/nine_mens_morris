package model;
/** 
 * The Player class represents a player in the game of Nine Men's Morris.
 * It contains the player's color, current phase of the game, board state,
 * and number of pieces placed.
 *
 * It provides methods for placing, removing, and moving pieces on the board,
 * advancing the game phase, and modifying the board state. The key methods are:
 *
 * - setPiece: Places a piece on the board if in the placing phase.
 * - removePiece: Removes a piece, advancing to end phase if applicable.
 * - movePiece: Moves a piece from one position to another.
 * - withGamePhase: Updates the player's current game phase.
 * - countUp: Increments number of pieces placed.
 * - modifyBoard: Helper to update board state.
 * @param color An Enum identifier. A player can be white or black.
 * @param state A GamePhase identifier. A player can be in the placing phase, the game phase, or the end phase.
 * During the Placing Phase, the player may only place pieces.
 * During the move phase, the player may only move one piece one edge at a time.
 * During the Endphase, the player is allowed move one piece anywhere, he want's to.
 * @param board A MillBoard object. A player can have a mill board.
 * @param counter An integer value that stores information about how many checkers the player has placed during 
 * the game. If the counter exceeds a maximum, the player changes it's game phase to the moving phase. 
 */
public record Player(Color color, GamePhase state, MillBoard board, int counter) {
  /**
   * Method of placing checkers on the player's board while checking as if the
   * player is still in it's placing
   * GamePhase. If he isn't, it is not possible to place a piece.
   * 
   * @param pos position, where you want to place the piece
   * @return With the return of this method, it can be evaluated whether the piece
   *         was successfully set or not.
   *         If the piece was not succesfull set, the method simply returns the
   *         player and an enum that says it wasn't
   *         possible.
   */
  Player setPiece(int pos) {
    return modifyBoard(pos, true).countUp();
  }

  /**
   * Method of removing pieces from a player's board while checking for the
   * player's GamePhase.
   * 
   * @param pos Position where you want to remove the piece
   * @return If the player is already in it's Moving GamePhase and would enter
   *         it's EndGame GamePhase after the
   *         removal of the piece, the player will be removed in this GamePhase
   *         with the removed piece. Otherwise, only
   *         the piece is removed.
   */
  Player removePiece(int pos) {
    return (modifyBoard(pos, false).board().dangerZone() && state() == GamePhase.Moving)
        ? modifyBoard(pos, false).withGamePhase(GamePhase.EndGame)
        : modifyBoard(pos, false);
  }
  int get_piece_amount(){
    return this.board().board().cardinality();
  }

  /**
   * This method simply moves a piece by deleting it at the old position and
   * creating it at the new position.
   * 
   * @param new_position The position, where the piece will be put on
   * @param old_position The position, where the piece comes from
   * @return simply returns a player with the modified board.
   */
  Player movePiece(int new_position, int old_position) {
    return modifyBoard(old_position, false).modifyBoard(new_position, true);
  }

  /**
   * The method sets the player's GamePhase to the desired GamePhase.
   * 
   * @param gamePhase describes the Gamephase of the Player
   * @return the newly constructed Player object with desired GamePhase
   */
  private Player withGamePhase(GamePhase gamePhase) {
    return new Player(this.color, gamePhase, this.board, this.counter);
  }

  /**
   * This method keeps track of how many checkers the player has already placed,
   * this method
   * changes the of the player to it's Moving GamePhase.
   * 
   * @return checks, if the Player has placed already enough pieces, to get into
   *         it's Moving
   *         or not and the returns a new player with the resulting GamePhase.
   */
  private Player countUp() {
    int count = this.counter + 1;
    return new Player(this.color, (count < 9 || this.state!=GamePhase.Placing)? this.state : GamePhase.Moving, this.board, (count <= 9)? count : this.counter);
  }

  /**
   * modifies the player board,
   * 
   * @param pos
   * @param value
   * @return a Player with the modifications will returned
   */
  private Player modifyBoard(int pos, boolean value) {
    return new Player(this.color(), this.state(), this.board().setBoard(pos, value), this.counter());
  }
}
