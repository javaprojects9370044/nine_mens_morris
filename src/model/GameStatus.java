package model;

/** This enum defines the general state of the game. */
public enum GameStatus {
  BlackWon,
  WhiteWon,
  InProgress;
}