package model;

/**
 * Defines the current phase of the game in which each player is currently
 * located.
 */
public enum GamePhase {
  Placing,
  Moving,
  EndGame;
}