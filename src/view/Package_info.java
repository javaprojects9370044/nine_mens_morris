package view;
/**
 * The view package contains classes related to the visual representation 
 * of the 9 Men's Morris game. The main view class is ProcessingView, 
 * which extends PApplet and implements the IView interface. This contains 
 * common functionality like drawing the game board, pieces, win screens 
 * etc. It has fields for the controller, player token positions, colors, 
 * font, and screen dimensions. It also defines the possible token positions 
 * on the board.
 * 
 * There are two concrete view classes that extend ProcessingView:
 * WhiteScreen sets the background color to white and foreground color to 
 * black
 * BlackScreen sets the background color to black and foreground color to 
 * white
 * The IView interface defines methods that views must implement like drawing 
 * the game, win screens, title screen etc.
 * Player_Token_Storage is a record that stores the x, y position and color 
 * of a player's token.
 * Double_Doubles is a simple record that holds two double values, likely 
 * representing x and y coordinates.
 * So in summary, the view classes handle displaying the game state and 
 * interface for the user. ProcessingView contains shared functionality while WhiteScreen and BlackScreen set different color schemes. IView defines required view methods. The records store data needed for rendering.
 */