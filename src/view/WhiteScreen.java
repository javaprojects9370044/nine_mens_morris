/**
 * WhiteScreen class that extends ProcessingView. 
 * Sets up a white background and black foreground for the view.
 * 
 * @param width Width of the view
 * @param height Height of the view 
 * @param token_size Size of tokens to render
*/
package view;

public class WhiteScreen extends ProcessingView {

    public WhiteScreen(int width, int height, float token_size) {
        super(width, height, token_size);
        this.backgroundColor = 255;
        this.frontgroundColor = 0;
    }

}