package view;

/**
 * Record that stores a pair of generic typed values.
 * @param <T> The first generic type
 * @param <G> The second generic type
 */
public record Simple_storage<T, G>(T x, G y) {
}