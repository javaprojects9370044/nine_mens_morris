/**
 * The IView interface defines methods for displaying the game view and interacting with the user.
 * 
 * This includes methods for:
 * - Finding token positions on the board based on x/y coordinates
 * - Updating the display of current token positions 
 * - Drawing the main game board, win screens, title screen, etc.
 * - Providing user feedback on game events (winning, removing pieces, jumping, etc.)
 */
package view;

import java.util.ArrayList;

import model.Simple_Result;

public interface IView {
    /**
     * Finds the position on the board for a token at the given x,y coordinates.
     * 
     * @param x_coordinate The x coordinate of the token
     * @param y_coordinate The y coordinate of the token
     * @return The position on the board, or null if no token is at that location
     */
    public Integer find_token_position(int x_coordinate, int y_coordinate);

    /**
     * Updates the display of token positions on the board
     * using the given list of current token positions.
     * @param current_used_Pieces The list of current token positions provided by the model
     */
    public void update_tokens(ArrayList<Simple_Result<Integer, Integer>> current_used_Pieces);

    /**
     * Draws the main game board UI.
     */
    public void drawGame();

    /**
     * Draws the win screen for the given player.
     * 
     * This will display a message indicating the player has won,
     * along with their name and the final game board state.
     * @param player_name The name of the player who won
     */
    public void draw_won(String player_name);

    /**
     * Draws the title screen UI.
     */
    public void drawTitleScreen();

    /**
     * Draws the impossible move screen for the given player.
     *
     * This will display a message indicating the player's attempted move
     * is not allowed by the rules of the game, along with their name.
     * @param player_name The name of the player who attempted an invalid move
     */
    public void draw_impossible(String player_name);

    /**
     * Draws the screen indicating the opponent's piece was removed,
     * showing the given player's name.
     *
     * @param player_name The name of the player who removed their opponent's piece.
     */
    public void draw_remove_opponent_piece(String player_name);

    /**
     * Draws the screen indicating the given player performed a jumping or moving
     * action.
     * 
     * This will display a message with the player's name and the type of action
     * they took.
     *
     * @param player_name The name of the player who jumped or moved their piece.
     */
    public void draw_jumping_or_moving(String player_name);
}