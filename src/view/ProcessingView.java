package view;
import java.lang.Math;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import processing.core.PApplet;
import processing.core.PFont;
import controller.*;
import model.Simple_Result;
/**
 * @param controller The controller to set
 * @param screen_width The width of the screen
 * @param screen_height The height of the screen
 * @param token_size The size of the tokens
 * @param font The font to use for rendering text
 * @param backgroundColor The background color of the screen
 * @param frontgroundColor The foreground color of the screen
 * @param player_tokens The list of token positions for each player
 * @param possible_moves The list of possible moves for each player
 * @param possible_token_positions_factors The list of factors to multiply the 
 * possible token positions by to get the actual token positions
 * @param coordinates The list of coordinates to draw the rectangles of the Board
 */
public abstract class ProcessingView extends PApplet implements IView {
    protected IController controller;
    protected ArrayList<Simple_storage<Simple_storage<Double,Double>,Integer>> player_tokens;
    protected int backgroundColor;
    protected int frontgroundColor;
    protected int screen_width;
    protected int screen_height;
    protected float token_size;
    protected PFont font;
    public double[][] possible_moves;
    protected double[][] possible_token_positions_factors={
        {0.06,0.06}, // 0
        {0.205,0.21}, // 1
        {0.383,0.389}, // 2
        {0.504,0.06}, // 3
        {0.504,0.21}, // 4
        {0.504,0.389}, // 5
        {0.932,0.06}, // 6
        {0.806,0.21}, // 7
        {0.619,0.389}, // 8
        {0.932,0.509}, // 9
        {0.806,0.509}, // 10
        {0.619,0.509}, // 11
        {0.932,0.931}, // 12
        {0.806,0.814}, // 13
        {0.619,0.624}, // 14
        {0.504,0.931}, // 15
        {0.504,0.814}, // 16
        {0.504,0.624}, // 17
        {0.06,0.931}, // 18
        {0.205,0.814}, // 19
        {0.383,0.624}, // 20
        {0.06,0.509}, // 21
        {0.205,0.509}, // 22
        {0.383,0.509} // 23
    };
    protected double[][] coordinates = {
        {0.05, 0.05, 0.88, 0.01},
        {0.05, 0.05, 0.01, 0.88},
        {0.05, 0.93, 0.88, 0.01},
        {0.93, 0.05, 0.01, 0.89},

        {0.2, 0.2, 0.6125, 0.01},
        {0.2, 0.2, 0.01, 0.6125},
        {0.2, 0.804, 0.6125, 0.01},
        {0.804, 0.2, 0.01, 0.6125},

        {0.375, 0.375, 0.25, 0.01},
        {0.375, 0.375, 0.01, 0.25},
        {0.375, 0.617, 0.25, 0.01},
        {0.617, 0.375, 0.01, 0.25},

        {0.5,0.05,0.01,0.335},
        {0.5,0.617,0.01,0.320},
        {0.05,0.5,0.335,0.01},
        {0.617,0.5,0.320,0.01}
    };
    /**
     * The method binds a pointer within the class instance using the IController interface to the 
     * transferred memory reference of the corresponding controller. As the IView interface is 
     * explicitly used here, a controller integrated in this way can only be used with the methods 
     * provided by the IView interface.
     * @param controller
     */
    public void set_controller(IController controller) {
        this.controller = controller;
    }
    /**
     * Finds the index of the token at the given screen coordinates.
     * 
     * Searches through the possible token positions, calculating the distance from
     * the given coordinates. Returns the index of the first position within the
     * token size radius, or -1 if no match found.
     * In fact, within the game, it's not even possible to make this method return -1. 
     * If you click on a position on the board that isn't available, this method won't 
     * even be triggered.
     * @param x_coordinate - The x coordinate of the mouse position.
     * @param y_coordinate - The y coordinate of the mouse position.
     * @return The index of the token at the given coordinates, or -1 if no match found.
     */
    public Integer find_token_position(int x_coordinate, int y_coordinate) {
        return IntStream.range(0, possible_moves.length)
                .parallel()
                .filter(i -> Math.hypot((float) x_coordinate - (float) possible_moves[i][0],
                        (float) y_coordinate - (float) possible_moves[i][1]) < token_size*(float)screen_width)
                .findFirst().orElse(-1);
    }

    /**
     * Adds a token to the player_tokens list at the given index and color.
     * 
     * @param index_of_token The index in the possible_moves array where to place
     *                       the token
     * @param color          The color (player) value for the new token
     */
    public void update_tokens(ArrayList<Simple_Result<Integer, Integer>> current_used_Pieces) {
        Thread thread = new Thread(() -> {
            this.player_tokens = current_used_Pieces.stream()
                    .map(current_piece -> new Simple_storage<>(
                            new Simple_storage<>(
                                    possible_moves[current_piece.game()][0],
                                    possible_moves[current_piece.game()][1]),
                            current_piece.move()))
                    .collect(Collectors.toCollection(ArrayList::new));
        });

        thread.start();
    }

    /**
     * The method draws the text on the screen.
     * @param text
     */
    private void drawText(String text, double size_factor, double x_coordinate, double y_coordinate) {
        textFont(font);
        fill(color(60,252,3));
        textSize((float)(size_factor*(this.screen_width+this.screen_height)));
        text(
            text,
            (float)(this.screen_width*x_coordinate), (float)(this.screen_height*y_coordinate));
    }
    /**
     * The method draws the board on the screen.
     */    
    private void draw_board() {
        background(backgroundColor);
        fill(frontgroundColor);
        Arrays.stream(coordinates)
                .forEach(cord -> {
                    rect(
                        (float)(cord[0]*(double)this.screen_width),
                        (float)(cord[1]*(double)this.screen_height),
                        (float)(cord[2]*(double)this.screen_width),
                        (float)(cord[3]*(double)this.screen_height));
                });
    }
    /**
     * Constructor of the class.
     * @param screen_width - The width of the screen.
     * @param screen_height - The height of the screen.
     * @param token_size - The size of the token.
     * @return The instance of the class.
    */
    public ProcessingView(int screen_width, int screen_height, float token_size) {
        this.screen_width = screen_width;
        this.screen_height = screen_height;
        this.token_size=token_size;
        this.possible_moves=new double[24][2];
        for(int i=0; i<possible_moves.length; i++){
            possible_moves[i][0]=possible_token_positions_factors[i][0]*screen_width;
            possible_moves[i][1]=possible_token_positions_factors[i][1]*screen_height;
        }
        this.player_tokens=new ArrayList<Simple_storage<Simple_storage<Double,Double>,Integer>>();
        setSize(screen_width, screen_height);
    }
    /**
     * Default method from Processing.
    */
    @Override
    public void Settings() {
    }
    public void setup() {
        background(backgroundColor);
        this.font=createFont("Georgia",16,true);
    }
    /**
     * This method is called by the PApplet.runSketch() method.
     * The draw() method of the processing framework is responsible for 
     * drawing the frames of the sketch - the visible part of the program - of the program.
     */
    @Override
    public void draw(){
        controller.nextFrame();
    }
    /**
     * The mousePressed() function is called once after every time a mouse button is pressed. 
     * The mouseButton variable (see the related reference entry) can be used to determine which button has been pressed.
     * 
     * Mouse and keyboard events only work when a program has draw(). Without draw(), the code 
     * is only run once and then stops listening for events.
     */
    @Override
    public void mousePressed() {
        controller.userInput(mouseX, mouseY);
        System.out.println("Mouse pressed"+(double)mouseX/(double)screen_width+" "+(double)mouseY/(double)screen_height);
    }
    @Override
    public void drawGame() {
        draw_board();
        if(player_tokens != null){
        for(Simple_storage<Simple_storage<Double,Double>,Integer> token : player_tokens){
            fill(token.y());
            ellipse(
                token.x().x().floatValue(), 
                token.x().y().floatValue(), 
                token_size*(float)screen_width, 
                token_size*(float)screen_height
                );
        }}
    }
    
    /**
     * Draws a message indicating the player has won.
     * 
     * @param player_name The name of the player who won.
     */
    public void draw_won(String player_name) {
        drawGame();
        drawText(player_name, 0.05, 0.2, 0.5);
    }

    /**
     * Draws text indicating the given player cannot make a valid move.
     * 
     * @param player_name The name of the player who cannot make a valid move.
     */
    public void draw_impossible(String player_name) {
        drawText(player_name, 0.05, 0.01, 0.5);
    }
    public void draw_remove_opponent_piece(String player_name){
        drawText(player_name,0.04,0.01,0.5);
    }

    /**
     * Draws the title screen with the game name and start prompt.
     * 
     * Draws the background color, then calls drawText() to display
     * the title text and start prompt at the specified coordinates.
     */
    @Override
    public void drawTitleScreen() {
        background(backgroundColor);
        drawText("Nine Men's Morris \n click to start", 0.05, 0.2, 0.5);
    }

    /**
     * Draws text indicating the given player is jumping or moving.
     * 
     * @param player_name The name of the player who is jumping or moving.
     */
    public void draw_jumping_or_moving(String player_name) {
        drawText(player_name, 0.03, 0.09, 0.4);
    };
}