/**
 * Blackscreen is a subclass of ProcessingView that sets a black background and white foreground color.
 * 
 * It takes in width, height and token_size parameters to instantiate the ProcessingView superclass.
 * 
 * The backgroundColor is set to black (0) and the foregroundColor is set to white (255).
 */
package view;

public class Blackscreen extends ProcessingView {

    public Blackscreen(int width, int height, float token_size) {
        super(width, height, token_size);
        this.backgroundColor = 0;
        this.frontgroundColor = 255;
    }
}