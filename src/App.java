import controller.Nine_mens_morris_controller;

import view.Blackscreen;
import view.WhiteScreen;
import model.Game;
import processing.core.PApplet;

public class App{
    
    private int screen_width;
    private int screen_height;

    public App(int screen_width, int screen_height) {
        this.screen_width = screen_width;
        this.screen_height = screen_height;
    }

    public static void main(String[] args) {
        
        App application = new App(1200, 600);
        
        var view = new WhiteScreen(application.screen_width, application.screen_height,(float)0.055);
        var controller = new Nine_mens_morris_controller();
        var model = Game.of();
        
        controller.set_view(view);
        view.set_controller(controller);
        controller.set_model(model);

        PApplet.runSketch(new String[] { "nine_mens_morris"}, view);
    }
    public App() {}

}
