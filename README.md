# nine men's morris in model view controller
This programme represents a nine men's Morris board game in Java using the Model View Controller pattern.
## functional scope
The scope of this programme includes
- A nine-man Morris board game that can be played on one screen. The two players should be able to take turns playing by interacting with their pieces on the board.
- A thread safe model: The whole model of the game will be implemented using Java records to make it as thread-safe as possible.
- a communication layer: You should be able to play with another player using Internet Protocol (IP).

## sate of the game:
The Game is fully functional and can be played with two players on one screen.  
Game AI included in the model, but not currently used.

## Getting Started
To run the project, simply open the project in vscode, go to the app.java file and click on the run code icon.
## How to play
Both players play the game by touching the game screen. The game is local 1v1.
## Folder Structure

The workspace contains two folders by default, where:

- `src`: the folder to maintain sources
- `lib`: the folder to maintain dependencies
- `doc`: the folder to maintain documentation files
- `test`: the folder to maintain test files

Meanwhile, the compiled output files will be generated in the `bin` folder by default.

> If you want to customize the folder structure, open `.vscode/settings.json` and update the related settings there.

## Dependency Management

The `JAVA PROJECTS` view allows you to manage your dependencies. More details can be found [here](https://github.com/microsoft/vscode-java-dependency#manage-dependencies).

## Jshell
To start Jshell, you should go to the `bin/' folder. There you'll have full access to the compiled class files. Then just type under linux into the terminal:
```
jshell
```
For example, if you want to play with the model and you're in the shell:
```
import model.*;
```
## Look and Feel of the game.
After extensive QA testing, it was discovered that the game was not very self-explanatory. To make the game more understandable, additional information has been added to the text output.
### Screenshots from the game
![A title screen of the game.](./images/title_screen.png)
![A Screenshot of an in game state.](./images/in_game_screen.png)
![A Screenshot, which displays, if you're allowed to remove an opponent piece.](./images/remove_piece.png)
![A Screenshot of the moving phase](./images/moving_phase.png)
![A Screenshot of the winning phase](./images/winning_screen.png)